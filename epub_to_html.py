#!/usr/bin/python3

import sys
import zipfile
import os
from io import StringIO  # for Python 2 import from StringIO instead
import xml.etree.ElementTree as ET
from re import sub

file=sys.argv[1]
path=file.split('.')[0]

if not os.path.exists(path):
	os.mkdir(path)

with zipfile.ZipFile(file, 'r') as zip_ref:
	zip_ref.extractall(path)

contdir = os.listdir(path+'/OEBPS')

toc = path+'/OEBPS/toc.ncx'
tocc = open(toc, "r")

it = ET.iterparse(StringIO(tocc.read()))
for _, el in it:
	_, _, el.tag = el.tag.rpartition('}') # strip ns
root = it.root

chapters=[]

for point in root.findall('navMap/navPoint'):
	text = point.find('navLabel/text').text
	url = point.find('content').attrib['src']
	chapters.append([text,url])

if not os.path.exists(path+'_html'):
	os.mkdir(path+'_html')

menu='<div id="menu">\n'
menu+='<h3 id="menutit">Indice</h3>\n'
menu+='<ul id="menucon">\n'
for chapter in chapters:
	menu+='<a class="chapter" href="'+chapter[1].replace('xhtml','html')+'">'
	menu+=chapter[0]
	menu+='</a>\n'
menu+='</ul>\n'
menu+='</div>\n'

for chapter in chapters:
	with open(path+'/OEBPS/'+chapter[1], "r") as input:
		with open(path+'_html/'+chapter[1].replace('xhtml','html'), "w") as output:
			hhtml=0
			for line in input:
				line=line.replace('&#160;','')
				line=line.replace(' ','')
				line=sub('<(https?[^\s]+)>', '<a href="\g<1>">\g<1></a>', line)

				if not hhtml:
					if '<html' in line:
						hhtml=1
						output.write('<html>\n')
				else:
					if '</body' in line:
						with open('eleuthera_foot.html', "r") as inputt:
							for linee in inputt:
								output.write(linee)
					output.write(line)
					if '<body' in line:
						with open('eleuthera_head.html', "r") as inputt:
							for linee in inputt:
								output.write(linee)
						with open('eleuthera.js.html', "r") as inputt:
							for linee in inputt:
								output.write(linee)
						with open('ccshare.js.html', "r") as inputt:
							for linee in inputt:
								output.write(linee)
						with open('eleuthera_snipcart_js.html', "r") as inputt:
							for linee in inputt:
								output.write(linee)
						output.write(menu)
					if '<head' in line:
						output.write('<meta charset="UTF-8">\n')
					if '<head' in line:
						output.write('<link rel="stylesheet" type="text/css" href="css/materiali.css"/>\n')

os.system('cp -r '+path+'/OEBPS/image '+path+'_html')
os.system('mkdir '+path+'_html/css')
os.system('cp -r fonts materiali.css '+path+'_html/css')
os.system('rm -r '+path)
