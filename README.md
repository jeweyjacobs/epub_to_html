# epub_to_html

very rough sketch.

this extracts xhtml chapters from epub, generates a table of contents and pastes it in as a menu in the individual html chapters.

tailored for indesign epubs for the moment.

## usage

```
python3 epub_to_html.py book.epub
```
